package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.ProjectGetByIndexRequest;
import ru.tsc.kirillov.tm.dto.response.ProjectGetByIndexResponse;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIndexCommand extends AbstractProjectShowCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отобразить проект по его индексу.";
    }

    @Override
    public void execute() {
        System.out.println("[Отображение проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest(getToken(), index);
        @NotNull final ProjectGetByIndexResponse response = getProjectEndpoint().getProjectByIndex(request);
        showProject(response.getProject());
    }

}
