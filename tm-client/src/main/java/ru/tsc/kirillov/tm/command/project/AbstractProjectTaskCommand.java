package ru.tsc.kirillov.tm.command.project;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.kirillov.tm.command.AbstractCommand;
import ru.tsc.kirillov.tm.enumerated.Role;

@Getter
@Component
public abstract class AbstractProjectTaskCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private IProjectTaskEndpoint projectTaskEndpoint;

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
