package ru.tsc.kirillov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.TaskListByProjectIdRequest;
import ru.tsc.kirillov.tm.dto.response.TaskListByProjectIdResponse;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class TaskListByProjectIdCommand extends AbstractTaskListCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-project-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отобразить список задач проекта.";
    }

    @Override
    public void execute() {
        System.out.println("[Список задач проекта]");
        System.out.println("Введите ID проекта:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(getToken(), projectId);
        @NotNull final TaskListByProjectIdResponse response = getTaskEndpoint().listTaskByProjectId(request);
        printTask(response.getTasks());
    }

}
