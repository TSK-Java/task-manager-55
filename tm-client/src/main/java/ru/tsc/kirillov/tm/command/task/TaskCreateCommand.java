package ru.tsc.kirillov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.TaskCreateRequest;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.util.Date;

@Component
public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Создание новой задачи.";
    }

    @Override
    public void execute() {
        System.out.println("[Создание задачи]");
        System.out.println("Введите имя:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        @NotNull final String description = TerminalUtil.nextLine();
        System.out.println("Введите дату начала:");
        @NotNull final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("Введите дату окончания:");
        @NotNull final Date dateEnd = TerminalUtil.nextDate();
        @NotNull final TaskCreateRequest request =
                new TaskCreateRequest(getToken(), name, description, dateBegin, dateEnd);
        getTaskEndpoint().createTask(request);
    }

}
